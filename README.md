Introduction
============
ParaViewEasyPluginBuilder is an open-source [docker][] based script to
facilitate the building of plugin for the binary release of [ParaView][].
This tool is developed by [Kitware SAS][].

[ParaView]: http://www.paraview.org
[docker]: https://www.docker.com
[Kitware SAS]: https://www.kitware.eu

TLDR
====

```
./run_build_plugin.sh -d /path/to/plugin/directory releaseTag
```

Supported Versions
==================

ParaViewEasyPluginBuilder supports all releases of ParaView starting from 5.10.0.
It support also nightly versions.

To check the availability of a version, just browse the [dockerhub repository tags][].

To build for an older version, you can use the [ParaViewPluginBuilder][] project.

[dockerhub repository tags]: https://hub.docker.com/r/kitware/paraview_org-plugin-devel/tags
[ParaViewPluginBuilder]: https://gitlab.kitware.com/paraview/paraview-plugin-builder

Building a plugin
=================

To build a plugin, first make sure that the targeted version of ParaView is available,
then use the `run_build_plugin` script.

As parameters, use `-d` option to specify the path that contains the plugin sources,
and specify the tag of the version of ParaView to build the plugin with.
Note that if no plugin directory is provided, the script tries to build the file `plugin.tgz`
in the script directory.

 * `run_build_plugin.sh -d /path/to/plugin/directory hashOrTag`
 * eg: `./run_build_plugin.sh -d /home/user/myPlugin 5.10.1`
 * eg: `./run_build_plugin.sh -d /home/user/myPlugin 454e6fca1f`


If specific CMake options have to be passed during the configuration of the plugin,
they can be specified in the `plugin.cmake` file.

The resulting build, containing the .so binaries will be outputed in the `output` directory.

About binary plugin compatibility
=================================

Once a plugin has been built for a specific release or nightly hash, it is assured to be
compatible only with this specific release or nightly. However, in most cases, a binary plugin
may load and work perfectly with every patch version of the release it has been built for.
e.g.: a plugin built for 5.10.0 may be compatible with 5.10.1 as well.

This is true until it is not. It can be because a plugin dependency has seen itself updated
in the new patch release or because an API has changed in a patch release. In this case, building
the plugin for this new release will, of course, resolve the problem.

The same can be said for the last release and the nightly build. However, since the very reason
to build a plugin for a nightly is to have access to new features ahead of time, then if these new features,
only available in the nightly, are used in the plugin, then it will, of course, be necessary to build the
plugin for this nightly hash or a more recent one.

License
=======

ParaViewEasyPluginBuilder is distributed under the OSI-approved BSD 3-clause License.
See [License.txt][] for details. For additional licenses, refer to the
[ParaView License][].

[License.txt]: License.txt
[ParaView License]: http://www.paraview.org/paraview-license/
